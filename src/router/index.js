import Vue from 'vue'
import Router from 'vue-router'
import main from '@/components/main'
import mainEn from '@/components/en/main'
import historia from '@/components/historia'
import map from '@/components/map'
import avisolegal from '@/components/avisolegal'
import avisolegalEn from '@/components/en/avisolegal'
import privacidad from '@/components/privacidad'
import privacidadEn from '@/components/en/privacidad'
import cookies from '@/components/cookies'
import condiciones from '@/components/condiciones'

Vue.use(Router)

export default new Router({
  //mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: main
    },
    {
      path: '/avisolegal',
      name: 'AvisoLegal',
      component: avisolegal
    },
    {
      path: '/privacidad',
      name: 'Privacidad',
      component: privacidad
    },
    {
      path: '/cookies',
      name: 'Cookies',
      component: privacidad
    },{
      path: '/condiciones',
      name: 'Condiciones',
      component: condiciones
    },{
      path: '/en/',
      name: 'English',
      component: mainEn
    },
    {
      path: '/avisolegalEn',
      name: 'AvisoLegalEn',
      component: avisolegalEn
    },
    {
      path: '/privacidadEn',
      name: 'PrivacidadEn',
      component: privacidadEn
    },

  ],
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  }
})
