var info;
export default info = {
  "Alava":{
    tiendas:[
      `Carrefour
      Calle Iruzaits-Gauna 1
      Vitoria`,
    ],
    bares:[
      `Café Pub Blason
      Calle Juan Lirola 22
      04004 Almería`
    ]
  },
  "Almería":{
    tiendas:[
      `Carrefour
      Avda del Mediterráneo,
      244 04006 Almería
      965942496`,
      `Carrefour
      Calle Iruzaits-Gauna 1
      Vitoria`,
      `Carrefour
      Ctra. Almerimar s/n
      El Ejido`,
      `Coviran`,
      `El Corte Inglés
      Paseo Pedro Ponce
      El Ejido`
    ]
  },
  "Barcelona":{
    tiendas:[
      `Colmado Casa Buendía
      C/Roger de Flor, 139
      08013 Barcelona
      Tlf- 934 63 87 01`,
      `Lafuente Colmado Quilez
      C/Juan Sebastian Bach, 20
      08021 Barcelona
      Tlf- 932 01 15 13`
    ]
  },
  "Cádiz":{
    tiendas:[
      `Bebidas Jerez
      Ctra. Nacional IV, Km. 640
      Jerez de la Frontera`,
      `El Corte Inglés
      C/ Bergantin
      Los Barrios`,
      `Eroski
      Ctra. Cadiz - Málaga, Km. 106,25
      Algeciras`,
      `Magerit Vinos
      C/ Fermin Salvochea 2
      Cádiz`,
      `Ya En Tu Casa
      C/ dela Corredera, 17
      Vejer de la Frontera`
    ],
    bares:[
      `Artichoke SL
      Paseo de la Rosaleda 20
      11405, Cadiz`,
    ]
  },
  "Córdoba":{
    tiendas:[
      `Bebidas Cordoba
      C/ Francisco Jimenez Carmona, Nave 4
      Puente Genil`,
      `El Corte Inglés
      Ronda de los Tejares 32
      Cordoba`
    ]
  },
  "Granada":{
    tiendas:[
      `Alcampo
      Ctra. Jaen s/n
      Granada`,
      `Alcampo
      Avda. Salobreña s/n
      Motril`,
      `Almano S.A. (Bodegas Mateos)
      Barrio San Sebastián, 60
      Teléfono: 958630508
      Almuñecar`,
      `Bodegas Mar
      Ctra. Almeria 16
      Motril`,
      `Carrefour
      C/ Tablas, 14
      Granada`,
      `Carrefour
      Avda. Barcelona, 13
      Granada`,
      `Carrefour
      Avda Juan Perez Arcas
      Cullar Baza`,
      `Carrefour
      Avda. De La Constitucion
      Granada`,
      `Carrefour
      Ctra. Armilla, S/N
      Granada`,
      `Carrefour
      Avda. Salobreña, 24
      Motril`,
      `Carrefour
      CC Kinepolis
      Pulianas`,
      `Carrefour
      Avda. Adalucia, 1
      Cadiar`,
      `Carrefour
      C/ Nueva De San Anton, 15
      Granada`,
      `Carrefour
      C/ Parraga, 18
      Granada`,
      `Carrefour
      Ctra. Almeria, 29
      Cadiar`,
      "Coviran",
      `Distribuciones Mariano Jimenez
      Pol. Ind. La Marcoba, Manzana 7,Parcela 7 y 8
      Teléfono: 958660420
      Guadix`,
      `El Corte Inglés
      Carrera De La Virgen, 20 - 22
      Granada`,
      `El Corte Inglés
      C/ Arabial, 97
      Granada`,
      `El Corte Inglés
      Camino De Ronda, 212
      Granada`,
      `GM Cash
      Carretera Almeria 1
      Cadiar`,
      `La Gloria
      Plaza Constitucion 8
      Cadiar`,
      `Makro
      Granada`
    ]
  },
  "Jaén":{
    tiendas:[
      `El Corte Inglés
      Avda. Madrid, 31
      Jaén`,
      `El Corte Inglés
      Plaza Del Ayuntamiento
      Linares`
    ]
  },
  "Las Palmas":{
    tiendas:[
      `Manuel Vega S.L.U.
      Urb. Ind. Salinetas C/Pastor, 17
      35214 Telde (Las Palmas)
      Tlf- 928 13 23 50`
    ]
  },
  "Madrid":{
    tiendas:[
      `Bodegas Siguero
      Calle María de Molina 70
      Madrid 28006`,
      `Manterqueria Andres
      Paseo De Los Olmos, 3
      Madrid`,
      `Licores Pinto
      C/Luis Sauquillo, 100
      28944 Fuenlabrada (Madrid)
      Tlf- 91 609 88 89`
    ],
    bares:[
      `Palma 33 SLU
      c/ Palma 33
      28004, Madrid`,
      `Acme Madrid No Digo Más SL
      Calle Velarde     11
      Madrid     28004`,
      `Restaurante Madrigal
      Calle Salvades 34
      Colmenar Viejo
      Madrid     28770`,
      `Credin SA
      Calle Bermúdez 44
      Madrid     28003`,
      `Alberto Santervas Sanz,
      Avenida Ferrol    22
      Madrid     28029`,
      `Cervecería La Fragua
      Calle Andrés Mellado 84
      Madrid     28015`,
      `York Zelanda Developments
      Calle Gaztambide 49
      Madrid     28015`,
      `Licores Caché SL
      Paseo     Esperanza 23
      Madrid     28045`,
      `Cafetería Sicilia
      Calle    Isaac Peral 44
      Madrid     28040`,
      `Bar Navacerrada
      Avenida Reina Victoria 50
      Madrid     28003`,
      `Bar Támesis
      Calle Los Vascos 3
      Madrid     28040`,
      `Bar Santa Elena
      Calle Los Vascos 5
      Madrid     28040`,
      `Bar Manolo
      Avenida Reina Victoria 30
      Madrid     28003`,
      `Bar Infantas 32 SL
      Calle Infantas 32
      Madrid     28004`,
      `Sánchez Escudero CB
      Calle Marqués de Urquijo 24
      Madrid     28008`,
      `María Adela Rodríguez
      Calle Goya 12
      Madrid     28001`,
      `Cervecería Bravas
      Calle Meléndez Valdés 62
      Madrid     28015`,
      `Van Gogh Café
      Calle Isaac Peral 4
      Madrid     28015`,
      `Potmac
      Calle Fernández de los Ríos 92
      Madrid     28015`,
      `Bar Malabar
      Plaza 2 de mayo 9
      Madrid     28002`,
      `Scruff Murphy´s
      Calle Palma 47
      Madrid     28002`,
      `Taberna Baztan
      Calle San Andrés 14
      Madrid     28002`,
      `Cervecería Boomerang
      Calle San Mateo 14
      Madrid     28004`,
      `Emiaj 2010 SL
      Calle Barco 26
      Madrid     28002`,
      `Nasanu SL
      Calle San Vicente Ferrer 4
      Madrid     28004`,
      `Novocari Hostelería SL
      Calle Santa Bárbara 11
      Madrid     28002`,
      `La Vía 2000 / Freeway
      Calle Corredera Alta de San Pablo 17  Madrid     28004`,
      `Mix Shakers Inversiones
      Calle La Reina 1
      Madrid     28004`,
      `Magnum 63 restauración
      Calle Barquillo 26
      Madrid     28004`,
      `Café de Los Austrias
      Calle Amnistía 12
      Madrid     28004`,
      `Comocomo Slow Food SL
      Calle Julián Camarillo 47
      Madrid     28004`,
      `Insolencia SL
      Calle Divino Pastor 21
      Madrid     28004`,
      `Amiguetes Ocio y Diversión
      Calle La Palma 38
      Madrid     28004`,
      `Karamiel
      Calle Hortaleza    4
      Madrid     28004`,
      `Spectra para la alimentación del mundo SL
      Calle     De la Reina 23
      Madrid     28004`,
      `Moliere Art Drinks
      Calle Hortaleza    67
      Madrid     28004`,
      `LA Nieta Carmela
      Calle Humilladero 3
      Madrid     28004`,
      `El Madroño
      Plaza P. Cerrada 7
      Madrid     28004`,
      `Taberna El Puerto de Motril
      Calle Lagasca 12
      Madrid     28001`,
      `Bar Alambique
      Calle Granadilla 23
      Majadahonda, Madrid 28220`,
      `Sala Circus SL
      Plaza De Santiago s/n
      San Lorenzo Del Escorial, Madrid 28200`,
    ]
  },
  "Málaga":{
    tiendas:[
      `Bebidas Marbella
      C/ Granito 22-24
      Marbella`,
      `Bebidas Marbella
      C/ Punta Alta 6-8
      Málaga`,
      `Bebidas Paris
      Bahia Blanca, 44-46
      Málaga`,
      `Bodegas Calderón
      C/ Decano Manuel Torriglia, 2
      Churriana 29140`,
      `Carrefour
      Ctra De Cadiz, Km 241
      Málaga`,
      `Carrefour
      C/ Arroyo De Totalan, S/N
      Rincón De La Victoria`,
      `Carrefour
      Avda. Simon Bolivar, S/N
      Málaga`,
      `Carrefour
      Avda. Los Pinillos S/N
      Torremolinos`,
      `El Corte Inglés
      Boulevard Alfonso Hohenlohe, 2
      Marbella`,
      `El Corte Inglés
      Crtra. De Cártama, Km. 2
      Málaga`,
      `El Corte Inglés
      Avda. Andalucía, 4
      Málaga`,
      `El Corte Inglés
      C/ Ramón Areces
      Puerto Banus`,
      `El Corte Inglés
      C/ Campanares, S/N
      Mijas-Costa`,
      `Eroski
      Avda. Juan Carlos I, S/N
      Velez Málaga`,
      `Eroski
      Avda. De La Aurora, S/N
      Málaga`,
      `Eroski
      Centro Comercial Ingenio
      Torre Del Mar`,
      `GM Cash
      Ronda 15-24. Urb Sierra Gorda
      Coin`
    ],
    bares:[
      `Taberna Matahambre
      Calle Francisco Cano 63
      Fuengirola, Málaga 29640`,
      `Complejo Latino SL
      Camino Viejo de Coín 3
      Mijas Costa, Málaga 29649`
    ]
  },
  "Sevilla":{
    tiendas:[
      `El Corte Inglés
      C/ Luis Montoto, 122
      Sevilla`,
      `El Corte Inglés
      Plaza Duque De La Victoria, 7
      Sevilla`,
      `El Corte Inglés
      C/ San Pablo, 1
      Sevilla`,
      `Bebidas La Giralda
      C/ Sabueso, 22`
    ]
  },
  "Carrefour":{
    tiendas: [
      "Bajo Peticion En Cualquiera De España"
    ]
  },
  "El Corte Inglés":{
    tiendas: [
      "Bajo Peticion En Cualquiera De España"
    ]
  },
  "Supermercados Champions":{
    tiendas: [
      "Bajo Peticion En Cualquiera De España"
    ]
  },
  "Supersol/Cahsdiplo":{
    tiendas: [
      "Bajo Peticion En Cualquiera De España"
    ]
  },
  "GM Cash":{
    tiendas: [
      "Bajo Peticion En Cualquiera De España"
    ]
  },
  "Covirán":{
    tiendas: [
      "En toda Andalucía y Extremadura"
    ]
  },
  "Toledo":{
    bares:[
      `David Jiménez Vázquez
      Avenida Francisco Aguirre 11
      Talavera de la Reina, Toledo 45600`
    ]
  },
  "Valencia":{
    bares:[
      `Taberna El Balconcillo
      Calle San Fernando 4
      Valencia, Valencia 46001`
    ]
  }
}
