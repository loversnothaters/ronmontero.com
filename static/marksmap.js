var info;
export default info = [
  {
    type: "bar",
    name: "Pub Tito Daniel's",
    latlo: { lat: 36.7425268, lng: -3.5199847 },
    tel: "+34 958 82 06 32",
    web: "https://www.facebook.com/titodaniels/"
  },
  {
    type: "store",
    name: "Ochoa Gourmet",
    latlo: { lat: 36.7156901, lng: -4.4293587 },
    tel: "+34 657 49 11 59",
    web: "https://www.facebook.com/OchoaGourmet/"
  },
  {
    type: "bar",
    name: "Restaurante Los Moriscos",
    latlo: { lat: 36.7166937, lng: -3.5603256 },
    tel: "+34 958 82 03 47",
    web: "https://moriscosgolf.com"
  },
  {
    type: "bar",
    name: "Cervecería Bar El Tapeo",
    latlo: { lat: 36.7365155, lng: -3.5910096 },
    tel: "",
    web: ""
  },
  {
    type: "bar",
    name: "Mezquita14",
    latlo: { lat: 37.1763696, lng: -3.6101612 },
    tel: "+34 663 12 96 62",
    web: "http://mezquita14.es"
  },
  {
    type: "bar",
    name: "Restaurante Las Conchas",
    latlo: { lat: 37.4646247, lng: -2.7528331 },
    tel: "+34 958 70 41 44",
    web: "http://restaurantelasconchas.com"
  },
  {
    type: "bar",
    name: "Taberna Sixto",
    latlo: { lat: 41.2220714, lng: -4.1759458 },
    tel: "+34 921 57 46 53",
    web: "https://www.facebook.com/TabernaSixtoFuentepelayo/"
  },
  {
    type: "bar",
    name: "Mesón Riscal",
    latlo: { lat: 41.1175856, lng: -4.2654019 },
    tel: "+34 921 56 02 89",
    web: "https://elriscal.com"
  },
  {
    type: "bar",
    name: "Restaurante Casa Román",
    latlo: { lat: 41.2968005, lng: -3.7499806 },
    tel: "+34 921 54 04 32",
    web: "https://casa-roman.es"
  },
  {
    type: "store",
    name: "La Garnacha",
    latlo: { lat: 40.9425186, lng: -4.1215193 },
    tel: "+34 659 91 61 81",
    web: ""
  },
  {
    type: "bar",
    name: "Marisqueria Hermes",
    latlo: { lat: 36.7456861, lng: -3.8779338 },
    tel: "+34 696 97 77 29",
    web: "https://www.facebook.com/pages/Cafeteria-Marisqueria-Hermes/413396989037425"
  },
  {
    type: "bar",
    name: "Escuela Superior de Hosteleria de Sevilla",
    latlo: { lat: 37.3942613, lng: -6.0100721 },
    tel: "+34 954 29 30 81",
    web: "https://esh.es"
  },
  {
    type: "bar",
    name: "Best Western Hotel Salobreña",
    latlo: { lat: 36.7430927, lng: -3.6249464 },
    tel: "+34 958 61 02 61",
    web: "https://bestwesternhotelsalobrena.com"
  },
  {
    type: "bar",
    name: "Chiringuito Espeto",
    latlo: { lat: 36.7165643, lng: -3.5500813 },
    tel: "+34 858 99 00 70",
    web: ""
  },
  {
    type: "store",
    name: "La Vinoteca",
    latlo: { lat: 36.744328, lng: -3.5178668 },
    tel: "+34 958 19 93 45",
    web: "https://la-vinoteca.es"
  },
  {
    type: "bar",
    name: "Chiringuito El Barco",
    latlo: { lat: 36.7437976, lng: -3.0244579 },
    tel: "+34 858 99 00 70",
    web: ""
  },
  {
    type: "store",
    name: "Bodega Mar",
    latlo: { lat: 36.7340271, lng: -3.5419031 },
    tel: "+34 958 60 30 13",
    web: "https://bodegasmar.com"
  },
  {
    type: "bar",
    name: "Chiringuito Sunahra Beach Club",
    latlo: { lat: 36.7165372, lng: -3.5488707 },
    tel: "+34 673 53 50 05",
    web: "https://sunahrabeachclub.com"
  },
  {
    type: "store",
    name: "Bodegas de la Cruz",
    latlo: { lat: 37.4966884, lng: -2.7584296 },
    tel: "Bodegas de la Cruz",
    web: ""
  },
]
